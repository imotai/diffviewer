'''
Created on 2013-11-24

@author: imotai
'''
from com.du.code.syntax.scaner import SyntaxScaner
import unittest


class Test(unittest.TestCase):


    def testSample1(self):
        source ="""public class Main{}"""
        file_name = "test.java"
        scaner = SyntaxScaner()
        tokens = list(scaner.scan(file_name, source))
        self.assertEqual(7,len(tokens))
        self.assertEqual("public",tokens[0][2])
        self.assertEqual("Token.Keyword.Declaration",str(tokens[0][1]))
        self.assertEqual("p",source[tokens[0][0]])
    def testSample2(self):
        source ="""public class Main{
        
        }"""
        file_name = "test.java"
        scaner = SyntaxScaner()
        tokens = list(scaner.scan(file_name, source))
        print tokens

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSample1']
    unittest.main()