'''
Created on 2013-11-24

@author: imotai
'''
from com.du.code.diff.scaner import ExpressionScaner
from com.du.code.syntax.scaner import SyntaxScaner
def c_diff():
    old_text = """public stativ void main(){
            System.out.printl("hello world")
            }"""
    new_text = """public static void main(){
            System.out.println("hello world")
            }"""
    scaner = ExpressionScaner()
    old_tokens,new_tokens = scaner.scan(old_text, new_text)

def c_syntax():
    source ="""public class Main{
        
    }"""
    file_name = "test.java"
    scaner = SyntaxScaner()
    tokens = list(scaner.scan(file_name, source))


if __name__ == '__main__':
    c_diff()
    c_syntax()
    pass