'''
Created on 2013-11-24

@author: imotai
'''


def c_diff():
    from com.du.code.diff.scaner import ExpressionScaner

    old_text = """public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }public stativ void main(){
            System.out.printl("hello world")
            }"""
    new_text = """public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }public static void main(){
            System.out.println("hello world")
            }"""
    scaner = ExpressionScaner()
    old_tokens,new_tokens = scaner.scan(old_text, new_text)
    return old_tokens,new_tokens
def c_syntax():
    from com.du.code.syntax.scaner import SyntaxScaner
    source =open("/Users/imotai/workspace/cooder/src/cooder/upload.py","r").read()
    file_name = "test.py"
    scaner = SyntaxScaner()
    tokens = list(scaner.scan(file_name, source))

    return tokens

if __name__ == '__main__':
    from IPython.parallel import Client
    c = Client()
    ar1 = c[1].apply_async(c_diff)
    ar2 = c[0].apply_async(c_syntax)
    ar1.get_dict()
    ar2.get_dict()
    pass