#! -*- coding:utf-8 -*- 
'''
Created on 2013-11-24

@author: imotai
'''
from com.du.code.diff.scaner import ExpressionScaner

import unittest


class Test(unittest.TestCase):
    
    
    
    def test_sample1(self):
        old_text = """public stativ void main(){
            System.out.println("hello world")
            }"""
        new_text = """public static void main(){
            System.out.println("hello world")
            }"""
        scaner = ExpressionScaner()
        old_tokens,new_tokens = scaner.scan(old_text, new_text)
        self.assertEqual(3, len(old_tokens)) 
        self.assertEqual(3, len(new_tokens))
        
        self.assertEqual("s", old_text[old_tokens[1][0]])
        self.assertEqual("stativ", old_tokens[1][2])
        self.assertEqual("Token.Replace", str(old_tokens[1][1]))
        
        self.assertEqual("s", new_text[new_tokens[1][0]])
        self.assertEqual("static", new_tokens[1][2])
        self.assertEqual("Token.Replace", str(new_tokens[1][1]))
    def test_sample2(self):
        old_text = """public stativ void main(){
            System.out.printl("hello world")
            }"""
        new_text = """public static void main(){
            System.out.println("hello world")
            }"""
        scaner = ExpressionScaner()
        old_tokens,new_tokens = scaner.scan(old_text, new_text)
        self.assertEqual(5, len(old_tokens)) 
        self.assertEqual(5, len(new_tokens))
        
        self.assertEqual("s", old_text[old_tokens[1][0]])
        self.assertEqual("s", new_text[new_tokens[1][0]])
        
        self.assertEqual("p", old_text[old_tokens[3][0]])
        self.assertEqual("p", new_text[old_tokens[3][0]])
        
        self.assertEqual("Token.Replace", str(old_tokens[3][1]))
    
    
if __name__ == "__main__":
    unittest.main()