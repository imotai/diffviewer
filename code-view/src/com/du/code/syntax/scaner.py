#! -*- coding:utf-8 -*-
'''
Created on 2013-11-24

@author: imotai
'''
from pygments.lexers import get_lexer_for_filename


class  SyntaxScaner(object):
    
    def scan(self,file_name,text):
        """
        use pygments implements the syntaxscaner
        """
        try:
            lexer = get_lexer_for_filename(file_name)
            tokens = lexer.get_tokens_unprocessed(text)
            return tokens
        except:
            return []

