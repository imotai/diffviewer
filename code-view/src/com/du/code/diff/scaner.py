#! -*- coding:utf-8 -*-
'''

Created on 2013-11-24
@author: imotai
'''
from com.du.code.diff.token import Equal, Delete, Replace, Insert
from difflib import SequenceMatcher
import re



# diff tag relationship
TAG_TOKEN = {'equal':Equal,
             'replace':Replace,
             'delete':Delete,
             'insert':Insert}
class  EncodeNoSupportException(Exception):
    pass
class  ExpressionScaner(object):
    """
    支持按照规则切分字符进行diff对比,
    只支持utf-8 编码文本，使用前请将文本
    转换为utf-8
    """
    def __init__(self,token_exprs=r'([^\W_]+|[\W_])',
                      min_match_ratio = 0.3,
                      min_match_size = 2):
        #切分文本正则表达式
        self.token_pattern = re.compile(token_exprs,re.U)
        #最小匹配率
        self.min_match_ratio = min_match_ratio
        #最小匹配的大小
        self.min_match_size = min_match_size 
    def scan(self,old_version_text,
                  new_version_text):
        
        
        """
        扫描两个文本，生成对比token
        """
        if not old_version_text or not new_version_text:
            return None
        old_char_index = 0
        new_char_index = 0
        old_tokens = []
        new_tokens = []
        #保留换行符
        old_text_lines = old_version_text.splitlines(True)
        new_text_lines = new_version_text.splitlines(True)
        matcher = SequenceMatcher(None,old_text_lines,new_text_lines)
        for tag,o1,o2,n1,n2 in matcher.get_opcodes():
            tmp_old_str = ''.join(old_text_lines[o1:o2])
            tmp_new_str = ''.join(new_text_lines[n1:n2])
            if tag == 'equal':
                continue
            else:
                tmp_old_tokens,tmp_new_tokens = self.scan_diff_text(tmp_old_str, 
                                                                    tmp_new_str, 
                                                                    old_char_index,
                                                                    new_char_index)
                old_tokens.extend(tmp_old_tokens)
                new_tokens.extend(tmp_new_tokens)
            
            old_char_index += len(tmp_old_str)
            new_char_index += len(tmp_new_str)
        return old_tokens,new_tokens 
    def scan_diff_text(self,old_diff_text,
                            new_diff_text,
                            old_char_index=0,
                            new_char_index=0):
        """
        扫描有区别文本，生成token
        """
        old_tokens = []
        new_tokens = []
        if old_diff_text and not new_diff_text:
            old_tokens.append((old_char_index,Delete,old_diff_text))
            return old_diff_text,new_tokens
        if new_diff_text and not old_diff_text:
            new_tokens.append((new_char_index,Insert,new_char_index))
        if old_diff_text and not isinstance(old_diff_text,unicode):
            try:
                old_diff_text = unicode(old_diff_text,"utf-8")
            except Exception,_:
                raise EncodeNoSupportException("""please make sure input line 
                is utf-8 string or unicode
                """)
        if new_diff_text and not isinstance(new_diff_text,unicode):
            try:
                new_diff_text = unicode(new_diff_text,"utf-8")
            except Exception,_:
                raise EncodeNoSupportException("""please make sure input line 
                is utf-8 string or unicode
                """)
        old_text_list = self.token_pattern.findall(old_diff_text)
        new_text_list = self.token_pattern.findall(new_diff_text)
        matcher = SequenceMatcher(None,old_text_list,new_text_list)
        for tag,o1,o2,n1,n2 in matcher.get_opcodes():
            old_str = ''.join(old_text_list[o1:o2])
            new_str = ''.join(new_text_list[n1:n2])
            if tag == "equal" or tag == "replace":
                old_tokens.append((old_char_index,
                                   TAG_TOKEN.get(tag),
                                   old_str))
                new_tokens.append((new_char_index,
                                   TAG_TOKEN.get(tag),
                                   new_str))
            elif tag == "insert":
                new_tokens.append((new_char_index,
                                   TAG_TOKEN.get(tag),
                                   new_str))
            elif tag == "delete":
                old_tokens.append((old_char_index,
                                   TAG_TOKEN.get(tag),
                                   old_str))
            old_char_index += len(old_str)
            new_char_index += len(new_str)
        return old_tokens,new_tokens
    
        
        
    