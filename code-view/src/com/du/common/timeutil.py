#encoding:utf-8
'''
Created on 2013-8-3
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import time
class Stopwatch(object):
    def __init__(self):
        self.is_started = False
        self.consume = 0
    def start(self):
        self.is_started = True
        self.start_time = time.time()
    def stop(self):
        self.is_started = False
        self.consume = time.time()-self.start_time
        return self.consume
    def pretty_formate(self):
        second = self.consume
        hour =  int(second/3600)
        second-=hour*3600
        minute=int(second/60)
        second-= minute*60
        return "%02d:%02d:%05.2f"%(hour,minute,second)
