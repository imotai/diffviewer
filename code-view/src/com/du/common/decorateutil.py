'''
Created on 2013-11-24

@author: imotai
'''
from com.du.common.timeutil import Stopwatch
def print_time_consume(func):
    def print_time_consume_wrapper(*args, **kwds):
        stopwatch = Stopwatch()
        stopwatch.start()
        ret = func(*args, **kwds)
        stopwatch.stop()
        print stopwatch.pretty_formate()
        return ret
    return print_time_consume_wrapper