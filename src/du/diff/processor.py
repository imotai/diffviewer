'''
Created on 2013-7-11

@author: innerp

'''
from difflib import SequenceMatcher
# do not change these 
TOKEN_OLD_STATUS =['D','M','N']
TOKEN_NEW_STATUS =['A','M','N']
DIFFLIB_DIFF_TAGS=['replace','insert','delete']
DIFFLIB_EQUAL_TAG='equal'
def process(old_text,new_text):
    """
    """
    if not old_text and not new_text:
        return [],[]
    if not old_text and new_text:
        return [],[(TOKEN_NEW_STATUS[0],0,len(new_text)+1)]
    if old_text and not new_text:
        return [(TOKEN_OLD_STATUS[0],0,len(old_text)+1)],[]
    old_tokens =[]
    new_tokens =[]
    old_text_lines = old_text.splitlines(True)
    new_text_lines = new_text.splitlines(True)
    matcher = SequenceMatcher(None,old_text_lines,new_text_lines)
    for tag,o1,o2,n1,n2 in matcher.get_opcodes():
        if tag == DIFFLIB_DIFF_TAGS[0]:
            old_line_start = len(''.join(old_text_lines[0:o1]))
            old_temp_lines_str ="".join(old_text_lines[o1:o2])
            new_line_start = len(''.join(new_text_lines[0:n1]))
            new_temp_lines_str ="".join(new_text_lines[n1:n2])
            old_temp_token,new_temp_tokens = process_char(old_temp_lines_str,old_line_start,
                                                          new_temp_lines_str,new_line_start)
            old_tokens.extend(old_temp_token)
            new_tokens.extend(new_temp_tokens)
            continue
        if tag == DIFFLIB_DIFF_TAGS[1]:
            new_line_start = len(''.join(new_text_lines[0:n1]))
            new_line_length = len(''.join(new_text_lines[n1:n2]))
            new_tokens.append((TOKEN_NEW_STATUS[0],new_line_start,new_line_start+new_line_length+1))
            continue
        if tag == DIFFLIB_DIFF_TAGS[2]:
            old_line_start = len(''.join(old_text_lines[0:o1]))
            old_line_length = len(''.join(old_text_lines[o1:o2])) 
            old_tokens.append((TOKEN_OLD_STATUS[0],old_line_start,old_line_start+old_line_length+1))
            continue
    return old_tokens,new_tokens  
def process_char(old_line,old_line_start,
                 new_line,new_line_start):
    old_tokens =[]
    new_tokens =[]
    matcher = SequenceMatcher(None,old_line,new_line)
    for tag, o1, o2, n1, n2 in matcher.get_opcodes():
        if tag ==DIFFLIB_EQUAL_TAG:
            continue
        old_status,new_status = __get_token_status(tag)
        if old_status in TOKEN_OLD_STATUS[0:2]:
            old_tokens.append((old_status,o1+old_line_start,
                                          o2+old_line_start))
        if new_status in TOKEN_NEW_STATUS[0:2]:
            new_tokens.append((new_status,n1+new_line_start,
                                          n2+old_line_start))
    return old_tokens,new_tokens
def __get_token_status(tag):
    if tag not in DIFFLIB_DIFF_TAGS:
        return
    if tag==DIFFLIB_DIFF_TAGS[0]:
        return TOKEN_OLD_STATUS[1],TOKEN_NEW_STATUS[1]
    if tag==DIFFLIB_DIFF_TAGS[1]:
        return TOKEN_OLD_STATUS[2],TOKEN_NEW_STATUS[0]
    if tag==DIFFLIB_DIFF_TAGS[2]:
        return TOKEN_OLD_STATUS[0],TOKEN_NEW_STATUS[2]
