'''
Created on 2013-7-13

@author: innerp
'''
from pygments.lexers.jvm import JavaLexer

def generate_token():
    file =open('/Users/innerp/workspace/disruptor/src/main/java/com/lmax/disruptor/EventProcessor.java','r')
    file_content = file.read()
    lexer = JavaLexer()
    tokens = lexer.get_tokens_unprocessed(file_content)
    for token in tokens:
        print token
        
if __name__=='__main__':
    generate_token()